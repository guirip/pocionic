/**
 * @singleton
 * @class class DecisionUtils
 */
DecisionUtils = function() {
	// Returning instance if object has been initialiazed
	if (typeof DecisionUtils.instance === 'object') {
		return DecisionUtils.instance;
	}

	var _this = this;

	/**
	 * @private
	 */
	var init = function(){
		DecisionUtils.instance = _this;
	};

	/**
	 * Get CSS class to display a code exec
	 * @param codeOk
	 */
	this.getClassCodeExec = function(codeOk){
		return 'codeExecution' + (codeOk === "1" ? "OK" : (codeOk === "0" ? "KO" : "Inconnu"));
	};

	init();
};

/**
 * Return DecisionUtils singleton
 */
DecisionUtils.getInstance = function(){
	return new DecisionUtils();
};
