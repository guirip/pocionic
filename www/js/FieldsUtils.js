/**
 * @class class FieldsUtils
 */
FieldsUtils = function() {
};

FieldsUtils.getAgeLabel = function(age, type) {
	var labelAge = "";
	if (type === "H") {
		if (age === 1) {
			labelAge = ENGINE_LABELS.victime_age_heure_singulier;
		} else {
			labelAge = ENGINE_LABELS.victime_age_heure_pluriel;
		}
	} else if (type === "J") {
		if (age === 1) {
			labelAge = ENGINE_LABELS.victime_age_jour_singulier;
		} else {
			labelAge = ENGINE_LABELS.victime_age_jour_pluriel;
		}
	} else if (type === "S") {
		if (age === 1) {
			labelAge = ENGINE_LABELS.victime_age_semaine_singulier;
		} else {
			labelAge = ENGINE_LABELS.victime_age_semaine_pluriel;
		}
	} else if (type === "M") {
		labelAge = ENGINE_LABELS.victime_age_mois;
	} else {
		if (age === 1) {
			labelAge = ENGINE_LABELS.victime_age_an_singulier;
		} else {
			labelAge = ENGINE_LABELS.victime_age_an_pluriel;
		}
	}
	return age + " " + labelAge;
};

FieldsUtils.getAgeFromBirthday = function(ddmmyyyy) {
	var DDMMYY = ddmmyyyy.split("/");
	var birthday = new Date(DDMMYY[2], DDMMYY[1], DDMMYY[0]);
	var today = new Date();
	var thisYear = 0;
	if (today.getMonth() < birthday.getMonth()) {
		thisYear = 1;
	} else if ((today.getMonth() === birthday.getMonth()) && today.getDate() > birthday.getDate()) {
		thisYear = 0;
	} else if ((today.getMonth() === birthday.getMonth()) && today.getDate() < birthday.getDate()) {
		thisYear = 1;
	}
	var year = today.getFullYear() - birthday.getFullYear() - thisYear;
	var month = today.getMonth() - birthday.getMonth();
	var day = today.getDate() - birthday.getDate();
	var age = {
		value : "ndef",
		type : "ndef"
	};

	if (year > 0) {
		age.value = year;
		age.type = 'A';
	} else if (month > 0) {
		age.value = month;
		age.type = 'M';
	} else {
		age.value = day;
		age.type = 'J';
	}
	return age;
};

FieldsUtils.getAgeFromBirthdayFormated = function(ddmmyyyy) {
	var ageFormated = FieldsUtils.getAgeFromBirthday(ddmmyyyy);
	var typeFormated = {
		"A" : "ans",
		"M" : "mois",
		"J" : "jours"
	};
	return ageFormated.value + " " + typeFormated[ageFormated.type];
};

FieldsUtils.Tel = function() {
};

FieldsUtils.Tel.format = function(tel) {
	if (tel === null) {
		return;
	}

	var formattedTel = "";
	var numbers = tel.match(/.{0,2}/g);
	try {
		for ( var i = 0; i < numbers.length; i++) {
			formattedTel += numbers[i] + " ";
		}
	} catch (e) {
		formattedTel = tel;
	}

	return formattedTel.trim();
};

FieldsUtils.Tel.unformat = function(tel) {
	if (tel === null) {
		return;
	}

	var unformattedTel = "";
	var numbers = tel.split(/\s/);

	try {
		for ( var i = 0; i < numbers.length; i++) {
			unformattedTel += numbers[i];
		}
	} catch (e) {
		unformattedTel = tel;
	}

	return unformattedTel;
};

FieldsUtils.lastDay = function(mois, annee) {
	var lgMois = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if ((annee % 4 === 0 && annee % 100 !== 0) || annee % 400 === 0) {
		lgMois[1] += 1;
	}
	return lgMois[mois]; // 0 < mois <11
};

/**
 * Fonction de controle de champs value => valeur a controler type => type de champ (alpha / num / decimal / alphanum / tel)
 */
FieldsUtils.controlField = function(value, type) {

	var test = {
		// alpha : "^[a-zA-ZÀ-ÿ '-_~()!\"\/.,|]*$",
		alpha : "^[a-zA-ZÀ-ÿ '-]*$",
		num : "^[0-9]*$",
		decimal : "^[1-9]+(\.[0-9][0-9]?)?$",
		poids : "^$|^[0-9]*(\.[0-9][0-9]?)?( +kg)?$",
		alphanum : "^[a-zA-ZÀ-ÿ0-9 ]*$",
		tel : "^$|^(0[1-9][0-9]{8})$",
		date : "^$|^(((0[1-9])|([1-2][0-9])|(3[0-1]))\/((0[1-9])|(1[0-2]))\/((19[0-9]{2})|(20[0-9]{2})))$",
		genre : "[mfi]",
		age : "^[0-9]+[jmsa]?"
	};

	return new RegExp(test[type]).test(value);
};

/**
 * Constante contenant tous les pattern
 */
FieldsUtils.REGEXP = {
	alpha : new RegExp("^[a-zA-ZÀ-ÿ '-.\s]*$"),
	num : new RegExp("^[0-9]*$"),
	decimal : new RegExp("^[1-9]+(\.[0-9][0-9]?)?$"),
	poids : new RegExp("^$|^[0-9]*(\.[0-9][0-9]?)?( +kg)?$"),
	alphanum : new RegExp("^[a-zA-ZÀ-ÿ0-9-.\s.]*$"),
	// tel : new RegExp("^0[1-9]([-. ]?[0-9][0-9][-. ]?[0-9][0-9][-. ]?[0-9][0-9][-.
	// ]?[0-9][0-9])$"),
	tel : new RegExp("^(\\+|0[1-9])([\\d-. ]*)$"),
	secu : new RegExp("[1-3][0-9]{2}[0-9]{2}(2[AB]|[0-9]{2})[0-9]{3}[0-9]{3}[0-9]{2}"),
	date : new RegExp("^$|^(((0[1-9])|([1-2][0-9])|(3[0-1]))\/((0[1-9])|(1[0-2]))\/((19[0-9]{2})|(20[0-9]{2})))$"),
	date_heure : new RegExp("^$|^(?:((?:0?[1-9])|(?:[1-2][0-9])|(?:3[0-1]))\/((?:0?[1-9])|(?:1[0-2]))\/((?:19[0-9]{2})|(?:20[0-9]{2}))) ((?:[0-1]?[0-9])|(?:2[0-4]))\:([0-5]?[0-9])\:?([0-5]?[0-9])?$"),
	date2 : new RegExp("/(^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.](19|20)\d\d$)/gm"),
	dateJJ_MM_AA : new RegExp("[0-9]{2}[/\.]?[0-9]{2}[/\.]?[0-9]{2}"),
	dateJJ_MM_AAAA : new RegExp("[0-9]{2}[\./]?[0-9]{2}[\./]?[0-9]{4}"),
	dateJJ_MM_AAAA_ref : new RegExp("[0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4}"),
	genre : new RegExp("[mfiMFI]"),
	age : new RegExp("^[0-9]+[jmsaJMSA]?"),
	streetNumber : new RegExp("^[0-9]+\\s?(BIS|Bis|bis|B|b|TER|Ter|ter|T|t)?\\s{1}")
};

/**
 * Parse a date from a string : dd/MM/yyyy hh:mm:ss
 * @return object {
 * 		jour : ...,
 * 		mois : ...,
 * 		annee : ...,
 * 		heures : ...,
 * 		minutes : ...,
 * 		secondes : ...
 * }
 * or null if string format is not OK
 */
FieldsUtils.parseDateHeure = function(string){
	var result = FieldsUtils.REGEXP.date_heure.exec(string);
	if (!result || result[0] === ""){
		return null;
	} else {
		return {
			jour : parseInt(result[1]),
			mois : parseInt(result[2]),
			annee : parseInt(result[3]),
			heures : parseInt(result[4]),
			minutes : parseInt(result[5]),
			secondes : (result[6] ? parseInt(result[6]) : 0)
		};
	}
};

/**
 * Function that verify if a field is valid
 */
FieldsUtils.validateField = function(patternExp, value) {
	if (!patternExp.test(value)) {
		return false;
	} else {
		return true;
	}
};
FieldsUtils.isDateValid = function(dateToTest) {
	return (Object.prototype.toString.call(dateToTest) === "[object Date]" && !isNaN(dateToTest.getTime()) && dateToTest.getTime() < (new Date()).getTime());
};

FieldsUtils.today = function(value) {
	var currentDate = new Date();
	return (currentDate.getUTCDate() < 10 ? "0" : "") + currentDate.getUTCDate() + "/" + (currentDate.getUTCMonth() + 1) + "/" + currentDate.getUTCFullYear();
};

FieldsUtils.formatDate = function(value) {
	var result = -1;
	var currentDate;
	var currentYear;
	var century;
	var dateToTest;
	if (FieldsUtils.REGEXP.num.test(value)) {
		currentDate = new Date();
		currentYear = currentDate.getFullYear().toString().substr(2, 2);
		century = "19";
		var year;
		var month;
		var day;
		if (value.length === 6) {
			year = value.substr(4, 2);
			month = value.substr(2, 2);
			day = value.substr(0, 2);
			if (currentYear > year) {
				century = "20";
			}
			if (FieldsUtils.isDateValid(new Date(century + year, month - 1, day))) {
				result = day + "/" + month + "/" + century + year;
			}
		} else if (value.length === 8) {
			year = value.substr(4, 4);
			month = value.substr(2, 2);
			day = value.substr(0, 2);

			if (FieldsUtils.isDateValid(new Date(year, month - 1, day))) {
				result = day + "/" + month + "/" + year;
			}
		} else {
			result = FieldsUtils.formatDateFromNbMs(value);
		}
		return result;
	} else if (FieldsUtils.REGEXP.dateJJ_MM_AAAA.test(value)) {
		if (value.indexOf(".") !== -1) {
			dateToTest = value.split(".");

		} else if (value.indexOf("/") !== -1) {
			dateToTest = value.split("/");

		}
		if (FieldsUtils.isDateValid(new Date(dateToTest[2], dateToTest[1] - 1, dateToTest[0]))) {
			result = dateToTest[0] + "/" + dateToTest[1] + "/" + dateToTest[2];
		}
		return result;
	} else if (FieldsUtils.REGEXP.dateJJ_MM_AA.test(value)) {
		currentDate = new Date();
		currentYear = currentDate.getFullYear().toString().substr(2, 2);
		century = "19";

		if (value.indexOf(".") !== -1) {
			dateToTest = value.split(".");
			if (currentYear > dateToTest[2]) {
				century = "20";
			}
		} else if (value.indexOf("/") !== -1) {
			dateToTest = value.split("/");
			if (currentYear > dateToTest[2]) {
				century = "20";
			}
		}
		if (FieldsUtils.isDateValid(new Date(century + dateToTest[2], dateToTest[1] - 1, dateToTest[0]))) {
			result = dateToTest[0] + "/" + dateToTest[1] + "/" + century + dateToTest[2];
		}
		return result;
	}

	return result;
};

/**
 * Returns actual date time string, format : dd/MM/yyyy hh:mm:ss
 */
FieldsUtils.formatDateTimeNow = function(){
	var date = new Date(),
		month = date.getMonth()+1;
	return ((date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "/"
		+ (month < 10 ? "0" + month : month) + "/"
		+ date.getFullYear().toString()
		+ " " + date.toLocaleTimeString());
};

/**
 * Returns a human-readable string for this date
 * 
 * @param value :
 *            a date object or a number of milliseconds
 */
FieldsUtils.formatDateFromNbMs = function(value) {
	var date;
	if (typeof value == "object") {
		date = value;
	} else if (value) {
		date = new Date();
		date.setTime(value);
	} else {
		return "";
	}
	var mm = date.getMonth() + 1;
	if (mm < 10) {
		mm = "0" + mm;
	}
	var dd = date.getDate();
	if (dd < 10) {
		dd = "0" + dd;
	}
	var hh = date.getHours();
	if (hh < 10) {
		hh = "0" + hh;
	}
	var min = date.getMinutes();
	if (min < 10) {
		min = "0" + min;
	}
	var sec = date.getSeconds();
	if (sec < 10) {
		sec = "0" + sec;
	}
	return dd + "/" + mm + "/" + date.getFullYear() + " à " + hh + "h" + min + "m" + sec;
};

/**
 * Put the first letter to the uppercase
 */
FieldsUtils.toTitleCase = function(str) {
	return str.replace(/\w\S*/g, function(txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
};

/**
 * Strip the accents of a string
 */
FieldsUtils.stripAccents = function(str) {
	var reAccents = /[àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ]/g;
	var replacements = 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY';
	return str.replace(reAccents, function(match) {
		return replacements[reAccents.source.indexOf(match)];
	});
};

/**
 * Analyze a text and find if there is medical constant in it to put them in a tab
 */
FieldsUtils.searchConstantes = function(text) {
	var constanteTable = [];
	var keys = [];
	var keysO = [];
	var constantesExp = new RegExp("(&amp;)[a-zA-Z]+\s*(=|:|\s)\s*([a-zA-Z]|[0-9])+\s*(<br \/>)+", "g");
	var etcomm = new RegExp("(&amp;)", "g");
	var br = new RegExp("(<br \/>)", "g");
	while ((constante = constantesExp.exec(text)) !== null) {
		var constanteStrip = constante[0];
		var keyValue = null;
		var keyValueJ = {
			key : null,
			value : null
		};
		text.replace(constanteStrip, "");
		constanteStrip = constanteStrip.replace(etcomm, "");
		constanteStrip = constanteStrip.replace(br, "");
		if (constanteStrip.indexOf("=") != -1) {
			keyValue = constanteStrip.split("=");
		} else if (constanteStrip.indexOf(":") != -1) {
			keyValue = constanteStrip.split(":");
		}
		if (constanteStrip.indexOf(" ") != -1) {
			keyValue = constanteStrip.split(" ");
		}
		/*
		 * keyValueJ.key = keyValue[0]; console.log(keyValueJ);
		 * 
		 * console.log("BEFORE TABLE"); console.log(constanteTable); constanteTable.unshift(keyValueJ); console.log("AFTER TABLE"); console.log(constanteTable); keyValueJ = null;
		 */
		constanteTable[keyValue[0]] = constanteStrip;
		keys.push(keyValue[0]);
	}

	var columns = [];
	$.each(ENGINE_LABELS.CONSTANTES, function(index1, categorie) {
		var col = [];
		col.push(categorie.title);
		$.each(categorie.listes, function(index2, cste) {
			$.each(keys, function(index, key) {
				if ($.inArray(key, cste.values) != -1) {
					col.push(constanteTable[key]);
					keys.splice(index, 1);
				}
			});
		});
		if (col.length > 1) {
			columns.unshift(col);
		}
	});

	if (keys.length > 0) {
		var col = [];
		$.each(keys, function(index, key) {
			col.unshift(constanteTable[key]);
		});
		col.unshift("Autres");
		columns.unshift(col);
	}

	var tableFinal = "<table class='constantes'>";
	tableFinal = tableFinal + "<caption>Les constantes de la note</caption>";
	tableFinal = tableFinal + "<tr>";

	var maxLength = 1;
	$.each(columns, function(index, colonne) {
		tableFinal = tableFinal + "<th>" + colonne[0] + "</th>";
		if (colonne.length > maxLength) {
			maxLength = colonne.length;
		}
	});

	tableFinal = tableFinal + "</tr>";

	for ( var i = 1; i < maxLength + 1; i++) {
		tableFinal = tableFinal + "<tr>";
		$.each(columns, function(index, colonne) {
			tableFinal = tableFinal + "<td>" + (colonne[i] ? colonne[i] : "") + "</td>";
		});
		tableFinal = tableFinal + "</tr>";
	}

	// console.log(tableFinal);
	kv = null;
	constanteTable = null;

	return tableFinal;
};

/**
 * Return a string representing a distance. Below 1000, result is in meters. e.g: "789 m" Below 30000, result is in kilometers and has 1 number after comma. e.g: "27,8 km" Above, result is in
 * kilometers only. e.g: "76 km"
 * 
 * @param nbMeters
 * @return {String}
 */
FieldsUtils.formatDistance = function(nbMeters) {
	if (nbMeters < 1000) {
		return nbMeters + " m";
	} else {
		return parseFloat(nbMeters / 1000).toFixed(nbMeters < 30000 ? 1 : 0) + " km";
	}
};

var secondesInDay = 86400;
var secondsInHour = 3600;
/**
 * 
 * @param seconds
 * @return {String}
 */
FieldsUtils.formatDuration = function(seconds) {
	var days = Math.floor(seconds / secondesInDay);
	var hours = Math.floor(seconds / (secondsInHour) - days * 24);

	var divisor_for_minutes = seconds % (secondsInHour);
	var minutes = Math.floor(divisor_for_minutes / 60);

	var divisor_for_seconds = divisor_for_minutes % 60;
	seconds = Math.ceil(divisor_for_seconds);

	return {
		"d" : days,
		"h" : hours,
		"m" : minutes,
		"s" : seconds
	};
};
