angular
  .module('starter.controllers', [])

  .config(
	function ($stateProvider,   $urlRouterProvider) {
	  $urlRouterProvider.otherwise('/otherwise');

      $stateProvider
		.state("dossiers", {
			url: "",
			templateUrl : "dossiers.html",
			controller : "DossierListCtrl"
		})
		.state("dossier", {
			url : "/dossier/:id",
			templateUrl : "dossier.html",
			controller : "DossierCtrl"
		})
		.state("decision", {
			url : "/decision/:id",
			templateUrl : "decision.html",
			controller : "DecisionCtrl"
		})
		.state("moyenengage", {
			url : "/moyenengage/:id",
			templateUrl : "fiche.html",
			controller : "MoyenEngageCtrl"
		})
		.state("vehiculeengage", {
			url : "/vehiculeengage/:id",
			templateUrl : "fiche.html",
			controller : "VehiculeEngageCtrl"
		})
		.state("personnelengage", {
			url : "/personnelengage/:id",
			templateUrl : "fiche.html",
			controller : "PersonnelEngageCtrl"
		})
		.state("otherwise", { url : 'dossiers' });
  })


  // LISTE DES DOSSIERS EN COURS
  .controller('DossierListCtrl', function($scope, $http, $location, $ionicPopup) {

	  $http.get(CONFIG.middlewareUrl + '/dossier/valides/cors').then(
		// Success
		function(response){
		   $scope.dossiers = response.data;
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving valid dossiers", error);
	  });

	  $scope.goDossier = function(id){
		$location.url("/dossier/"+id);
	  };
  })


  // AFFICHAGE D'UN DOSSIER
  .controller('DossierCtrl', function($scope, $http, $stateParams, $location, $ionicPopup) {

	// Récupération du dossier
	$http.get(CONFIG.middlewareUrl + '/dossier/' + $stateParams.id + '/cors').then(
		// Success
		function(response){
		   $scope.dossier = response.data;
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving dossier " + $stateParams.id, error.status);
	 });

	 // Récupération des décisions
	 $http.get(CONFIG.middlewareUrl + '/decision/dossier/' + $stateParams.id + '/cors').then(
		// Success
		function(response){
		   $scope.decisions = response.data;
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving decisions for dossier " + $stateParams.id,  error);
	 });

	 $scope.goDecision = function(id){
		$location.url("/decision/"+id);
	 };
  })


  // AFFICHAGE D'UNE DECISION
  .controller('DecisionCtrl', function($scope, $http, $stateParams, $location, $ionicPopup) {
	 $http.get(CONFIG.middlewareUrl + '/decision/' + $stateParams.id + '/cascade/cors').then(
		// Success
		function(response){
		   $scope.decision = response.data;
		   $scope.dateFormated = FieldsUtils.formatDateFromNbMs(parseInt(response.data.decDate));
		   $scope.scenarioNeedMoyen = response.data.codifScenarioDTO.scenarioDTO.scIntMoyen == 1 ? "oui" : "non";
		   $scope.scenarioNeedVehicule = response.data.codifScenarioDTO.scenarioDTO.scIntMoyen == 1 ? "oui" : "non";
		   $scope.scenarioNeedPersonnel = response.data.codifScenarioDTO.scenarioDTO.scIntMoyen == 1 ? "oui" : "non";
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving decision " + $stateParams.id, error);
	 });

	 $scope.getMenSuffixe = function(men){
	 	return (men.moyenSecoursDTO.classe ? men.moyenSecoursDTO.classe + "." : "")
	 			+ (men.moyenSecoursDTO.type ? men.moyenSecoursDTO.type + "." : "");
	 };
	 $scope.getVenSuffixe = function(ven){
		 return (ven.venEtat ? ven.venEtat + "." : "");
	 };

	 $scope.goMoyenEngage = function(id){
		$location.url("/moyenengage/"+id);
	 };
	 $scope.goVehiculeEngage = function(id){
		$location.url("/vehiculeengage/"+id);
	 };
	 $scope.goPersonnelEngage = function(id){
		$location.url("/personnelengage/"+id);
	 };
  })


  // AFFICHAGE D'UN MOYEN ENGAGE
  .controller('MoyenEngageCtrl', function($scope, $http, $stateParams, $ionicPopup) {
	 $http.get(CONFIG.middlewareUrl + '/moyenengage/' + $stateParams.id + '/cors').then(
		// Success
		function(response){
			var fiche = new FicheMoyenEngageHelper(response.data);
			$scope.ficheTitle = fiche.getTitle();
			$scope.ficheHtml = fiche.getGeneralHtml();
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving moyen engage " + $stateParams.id, error);
	 });
  })


  // AFFICHAGE D'UN VEHICULE ENGAGE
  .controller('VehiculeEngageCtrl', function($scope, $http, $stateParams, $ionicPopup) {
	 $http.get(CONFIG.middlewareUrl + '/vehiculeengage/' + $stateParams.id + '/cors').then(
		// Success
		function(response){
			var fiche = new FicheVehiculeEngageHelper(response.data);
			$scope.ficheTitle = fiche.getTitle();
			$scope.ficheHtml = fiche.getGeneralHtml();
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving vehicule engage " + $stateParams.id, error);
	 });
  })

  
  // AFFICHAGE D'UN PERSONNEL ENGAGE
  .controller('PersonnelEngageCtrl', function($scope, $http, $stateParams, $ionicPopup) {
	 $http.get(CONFIG.middlewareUrl + '/personnelengage/' + $stateParams.id + '/cors').then(
		// Success
		function(response){
			var fiche = new FichePersonnelEngageHelper(response.data);
			$scope.ficheTitle = fiche.getTitle();
			$scope.ficheHtml = fiche.getGeneralHtml();
	    },
	    // Failure
	    function(error){
	    	showError($ionicPopup, "Error retrieving personnel engage " + $stateParams.id, error);
	 });
  });


/**
 * Generic way to display an error
 * @param message
 * @param error
 */
var showError = function($ionicPopup, message, error){
	$ionicPopup.alert({
		title: 'Error',
		okType: 'button-assertive',
	 	template: message + " : " + (error.status == 0 ? "couldn't reach server (" + error.status + ")" : error.status)
	});
};
