/**
 * HTTP related functions
 *
 * @class
 * @constructor
 */
function HTTP() {
}

/**
 * Default Timeout for network request.
 * @public
 */
HTTP._defaultTimeout = 60000;

/**
 * Sends a HTTP request
 *
 * @param {String} method the method to be called
 * @param {String} url the endpoint URL
 * @param {Array} headers the headers of the HTTP request
 * @param {Object} payload the payload of the HTTP request
 * @param {Function} success the success function to be called in case of success
 * @param {Function} errorCallback the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.request = function (method, url, headers, payload, success, errorCallback, timeout) {
    var _this = this;

    var errorSent = false;
    var error = function () {
        if (abortTimeout) clearTimeout(abortTimeout);
        request.abort();
        if (!errorSent) {
            errorSent = true;
            if (typeof errorCallback === 'function') errorCallback.apply(_this, arguments);
        }
    };

    var request = new XMLHttpRequest();
    request.addEventListener('error', error, false);

    if (method === "DELETE") {
        request.open("POST", url, true);
        request.setRequestHeader("X-HTTP-Method-Override", "DELETE");
    } else {
        request.open(method, url, true);
    }

    if (timeout) {
        var abortTimeout = setTimeout(error, timeout);
    }

    request.onreadystatechange = function () {
        if (request.readyState == 3) {
            if (request.status >= 400) {
                error(request.status);
            }
        }
        else if (request.readyState == 4) {
            if (abortTimeout) clearTimeout(abortTimeout);
            if ((request.status >= 200 && request.status < 300) || request.status == 304) {
                if (typeof success === 'function') success(request);
            } else {
                error(request.status);
            }
        }
    };

    if (headers) {
        headers.forEach(function (header) {
            request.setRequestHeader(header.name, header.value);
        });
    }

    request.send(payload);
};

/**
 * Sends a JSON request
 *
 * @param {String} method the method to be called
 * @param {String} url the endpoint URL
 * @param {Array} headers the headers of the HTTP request
 * @param {Object} payload the payload of the HTTP request
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.requestJSON = function (method, url, headers, payload, success, error, timeout) {
    if (method === 'POST') {
        if (!headers) headers = [];
        headers.push({name:'Content-Type', value:'application/json'});
    }

    HTTP.request(method, url, headers, method === 'POST' ? JSON.stringify(payload) : null, function (request) {
        if (typeof success === 'function') {
            var response;
            try {
                response = JSON.parse(request.responseText);
            } catch (err) {
            }
            success(response);
        }
    }, error, timeout);
};

/**
 * Sends an XML request
 *
 * @param {String} method the method to invoke
 * @param {String} url the endpoint URL
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.requestXML = function (method, url, success, error, timeout) {
    HTTP.request(method, url, null, null, function (request) {
        if (typeof success === 'function') success(request.responseXML);
    }, error, timeout);
};

/**
 * Sends a GET request for text stream
 *
 * @param {String} url the endpoint URL
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.getTEXT = function (url, success, error, timeout) {
    HTTP.request('GET', url, null, null, function (request) {
        if (typeof success === 'function') success(request.responseText);
    }, error, timeout);
};

/**
 * Sends a GET request for text stream
 *
 * @param {String} url the endpoint URL
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.get = function (url, success, error, timeout) {
    HTTP.request('GET', url, null, null, success, error, timeout);
};

/**
 * Sends a GET request for JSON stream
 *
 * @param {String} url the endpoint URL
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.getJSON = function (url, success, error, timeout) {
    HTTP.requestJSON('GET', url, null, null, success, error, timeout);
};

/**
 * Sends a POST request for JSON stream
 *
 * @param {String} url the URL endpoint
 * @param {Object} payload the payload of the HTTP request
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.postJSON = function (url, payload, success, error, timeout) {
    HTTP.requestJSON('POST', url, null, payload, success, error, timeout);
};

/**
 * Sends a POST request for TEXT stream
 *
 * @param {String} url the URL endpoint
 * @param {Object} payload the payload of the HTTP request
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.post = function (url, payload, success, error, timeout) {
    HTTP.request('POST', url, null, payload, success, error, timeout);
};

/**
 * Sends a DELETE request
 *
 * @param {String} url the URL endpoint
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 */
HTTP.deleteJSON = function (url, success, error) {
    HTTP.requestJSON('DELETE', url, null, null, success, error);
};

/**
 * Sends a GET request for XML
 *
 * @param {String} url the URL endpoint
 * @param {Function} success the success function to be called in case of success
 * @param {Function} error the success function to be called in case of error
 * @param {Number} timeout time in milliseconds to wait for a response. Optional
 */
HTTP.getXML = function (url, success, error, timeout) {
    HTTP.requestXML('GET', url, success, error, timeout);
};
