/**
 * Fiche d'un personnel engagé
 * @author GRI
 * @param dto
 * @returns
 */
function FichePersonnelEngageHelper(dto) {
	"use strict";

	var _this = this,
		penDTO,
		tplData = {
			col1Label : ENGINE_LABELS.fiche_effecteur_col1_label,
			qualifLabel : ENGINE_LABELS.fiche_effecteur_qualif_label,
			derniereDispoLabel : ENGINE_LABELS.fiche_effecteur_derniere_dispo_label,
			moyenRattachementLabel : ENGINE_LABELS.fiche_effecteur_moyen_rattachement_label,
			telephonesLabel : ENGINE_LABELS.fiche_effecteur_telephones_label,
			telephoneLabel : ENGINE_LABELS.fiche_effecteur_telephone_label,
			telephonerLabel : ENGINE_LABELS.fiche_effecteur_telephoner_label,
			posteLabel : ENGINE_LABELS.fiche_effecteur_poste_label,
			bipLabel : ENGINE_LABELS.fiche_effecteur_bip_label,
			col2Label : ENGINE_LABELS.fiche_effecteur_col2_label,
			vehAssocieLabel : ENGINE_LABELS.fiche_effecteur_veh_associe_label,
			penDateLabel : ENGINE_LABELS.fiche_effecteur_selection_label,
			penDateDebIntLabel : ENGINE_LABELS.fiche_effecteur_debut_int_label,
			penDateFinIntLabel : ENGINE_LABELS.fiche_effecteur_fin_int_label,
			isPhoneListDisplayed : function(){
				var test = (this.penDTO.personnelDTO.perTelNum
						|| this.penDTO.personnelDTO.perPosteNum
						|| this.penDTO.personnelDTO.perBip);
				return test !== undefined && test !== null;
			},
			getFormatedTel : function(){
				return (penDTO.personnelDTO.perTelNum ? FieldsUtils.Tel.format(penDTO.personnelDTO.perTelNum) : "");
			}
		};
	this.alive = true;
	this.title = null;
	this.infoGenerale = null;
	this.generalHtml = null;

	/**
	 * Initialise les données d'une fiche pour le véhicule engagé courant
	 * @private
	 */
	var init = function(){
		setDTO(dto);
		generateTitle();
		generateInfoGenerale();
		generateGeneralHtml();
	};

	/**
	 * 
	 * @private
	 */
	var setDTO = function(dto){
		penDTO = dto;
		tplData.penDTO = dto;
	};

	/**
	 * Get text displayed on button
	 * @private
	 */
	var generateTitle = function(){
		_this.title = (penDTO.personnelDTO.perNom + " - " + penDTO.personnelDTO.perTQualif);
	};

	/**
	 * Generate text displayed between button and tabs
	 * @private
	 */
	var generateInfoGenerale = function(){
		_this.infoGenerale = {
			ligne1 :
				  (penDTO.personnelDTO.perNom      ? penDTO.personnelDTO.perNom            : "")
				+ (penDTO.personnelDTO.perTQualif  ? ", " + penDTO.personnelDTO.perTQualif : "")
				+ (penDTO.personnelDTO.perCode     ? ", " + penDTO.personnelDTO.perCode    : "")
		};
	};

	/**
	 * Return info generale label for title attribute
	 */
	this.getInformationTooltip = function(){
		return _this.infoGenerale.ligne1;
	};

	/**
	 * Return html content for info generale
	 */
	this.getInformationHtml = function(){
		return "<div>" + _this.infoGenerale.ligne1 + "</div>";
	};

	/**
	 * Generate HTML content for "General" tab
	 * @private
	 */
	var generateGeneralHtml = function(){
		_this.generalHtml = ich.tplFichePenGeneral(tplData);
	};

	/**
	 * Return the menId related to this object
	 */
	this.getMenId = function(){
		return penDTO.penRMen;
	};

	/**
	 * Return the venId related to this object
	 */
	this.getVenId = function(){
		return penDTO.penRVen;
	};

	/**
	 * Get button label
	 */
	this.getTitle = function(){
		return _this.title;
	};

	/**
	 * Returns HTML content for "General" tab
	 */
	this.getGeneralHtml = function(){
		return $("<div />").html(_this.generalHtml).html();
	};

	init();
}
