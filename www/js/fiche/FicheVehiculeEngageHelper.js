/**
 * Fiche d'un véhicule engagé
 * @author GRI
 * @param dto
 * @returns
 */
function FicheVehiculeEngageHelper(dto) {
	"use strict";

	var _this = this, 
		venDTO,
		tplData = {
			etatLabel : ENGINE_LABELS.fiche_effecteur_ETAT_label,
			col1Label : ENGINE_LABELS.fiche_effecteur_col1_label,
			derniereDispoLabel : ENGINE_LABELS.fiche_effecteur_derniere_dispo_label,
			telephonesLabel : ENGINE_LABELS.fiche_effecteur_telephones_label,
			telephoneLabel : ENGINE_LABELS.fiche_effecteur_telephone_label,
			telephonerLabel : ENGINE_LABELS.fiche_effecteur_telephoner_label,
			retourUmhLabel : ENGINE_LABELS.fiche_effecteur_retour_umh_label,
			sortieBlancheLabel : ENGINE_LABELS.fiche_effecteur_sortie_blanche_label,
			col2Label : ENGINE_LABELS.fiche_effecteur_col2_label,
			codeExecLabel : ENGINE_LABELS.fiche_effecteur_code_execution_label,
			venDateLabel : ENGINE_LABELS.fiche_effecteur_selection_label,
			venDateDebIntLabel : ENGINE_LABELS.fiche_effecteur_order_depart_label,
			venDateDepartLabel : ENGINE_LABELS.fiche_effecteur_depart_garage_label,
			venDateArrLieu1Label : ENGINE_LABELS.fiche_effecteur_arrive_patient_label,
			venDateDepLieu1Label : ENGINE_LABELS.fiche_effecteur_depart_lieu_label,
			venDateArrDestLabel : ENGINE_LABELS.fiche_effecteur_arr_depose_patient_label,
			venDateDepDestLabel : ENGINE_LABELS.fiche_effecteur_dep_depose_patient_label,
			venDateFinMedLabel : ENGINE_LABELS.fiche_effecteur_fin_medicalisation_label,
			venDateDispoLabel : ENGINE_LABELS.fiche_effecteur_dispo_radio_label,
			venDateFinIntLabel : ENGINE_LABELS.fiche_effecteur_fin_int_label,
			isPhoneListDisplayed : function(){
				var test = (this.venDTO.vehicule.vehTel || venDTO.vehicule.VehiculeRadioDTOs);
				return test !== undefined && test !== null;
			},
			getFormatedTel : function(){
				return (venDTO.vehicule.vehTel ? FieldsUtils.Tel.format(venDTO.vehicule.vehTel) : "");
			},
			isRetourUmh : function(){
				return this.venDTO.venRetourUmh === "1";
			},
			isSortieBlanche : function(){
				return this.venDTO.venSortieBlanche === "1";
			},
			getClassCodeExecution : function(){
				return DecisionUtils.getInstance().getClassCodeExec(this.venDTO.venOk);
			},
			getCodeExec : function(){
				return this.venDTO.venExecution ? this.venDTO.venExecution : "?";
			},
		};
	this.alive = true;
	this.title = null;
	this.infoGenerale = null;
	this.generalHtml = null;

	/**
	 * Initialise les données d'une fiche pour le véhicule engagé courant
	 * @private
	 */
	var init = function(){
		setDTO(dto);
		generateTitle();
		generateInfoGenerale();
		generateGeneralHtml();
	};

	/**
	 * 
	 * @private
	 */
	var setDTO = function(dto){
		venDTO = dto;
		tplData.venDTO = dto;
	};

	/**
	 * Get text displayed on button
	 * @private
	 */
	var generateTitle = function(){
		_this.title = venDTO.vehicule.vehNom; //ich.tplFicheVenBtnTitle(tplData).html();
	};

	/**
	 * Generate text displayed between button and tabs
	 * @private
	 */
	var generateInfoGenerale = function(){
		var veh = venDTO.vehicule;
		_this.infoGenerale = {
			ligne1 :
				  (veh.vehCode  ? veh.vehCode + ", " : "")
				+ (veh.vehType  ? veh.vehType + ", " : "")
				+ (veh.vehNom   ? veh.vehNom + " - " : "")
				+ (veh.vehImmat ? veh.vehImmat + " " : ""),
			rattacheLabel : ENGINE_LABELS.fiche_effecteur_rattache_a_text,
			ligne2 :
				  (veh.moyenSecoursDTO.nom        ? veh.moyenSecoursDTO.nom + ", "       : "")
				+ (veh.moyenSecoursDTO.numVoie    ? veh.moyenSecoursDTO.numVoie + " "    : "")
				+ (veh.moyenSecoursDTO.nomVoie    ? veh.moyenSecoursDTO.nomVoie + ", "   : "")
				+ (veh.moyenSecoursDTO.etare      ? veh.moyenSecoursDTO.etare + ", "     : "")
				+ (veh.moyenSecoursDTO.codePostal ? veh.moyenSecoursDTO.codePostal + " " : "")
				+ (veh.moyenSecoursDTO.commune    ? veh.moyenSecoursDTO.commune          : "")
		};
		tplData.infoGenerale = _this.getInformationHtml();
	};

	/**
	 * Return info generale label for title attribute
	 */
	this.getInformationTooltip = function(){
		return _this.infoGenerale.ligne1 + _this.infoGenerale.rattacheLabel + _this.infoGenerale.ligne2;
	};

	/**
	 * Return html content for info generale
	 */
	this.getInformationHtml = function(){
		return $("<div />").html(ich.tplFicheVenInformation(_this.infoGenerale)).html();
	};

	/**
	 * Generate HTML content for "General" tab
	 * @private
	 */
	var generateGeneralHtml = function(){
		_this.generalHtml = ich.tplFicheVenGeneral(tplData);
	};

	/**
	 * Return the menId related to this object
	 */
	this.getMenId = function(){
		return venDTO.venRMen;
	};

	/**
	 * Get button label
	 */
	this.getTitle = function(){
		return _this.title;
	};

	/**
	 * Returns HTML content for "General" tab
	 */
	this.getGeneralHtml = function(){
		return $("<div />").html(_this.generalHtml).html();
	};

	init();
}
