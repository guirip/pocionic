/**
 * Fiche d'un moyen engagé
 * @author GRI
 * @param dto
 * @returns
 */
function FicheMoyenEngageHelper(dto) {
	"use strict";

	var _this = this,
		menDTO,
		faxRegExp = new RegExp("[Ff][Aa][Xx]"),
		tplData = {
			col1Label : ENGINE_LABELS.fiche_effecteur_col1_label,
			derniereDispoLabel : ENGINE_LABELS.fiche_effecteur_derniere_dispo_label,
			telephonesLabel : ENGINE_LABELS.fiche_effecteur_telephones_label,
			faxLabel : ENGINE_LABELS.fiche_effecteur_fax_label,
			col2Label : ENGINE_LABELS.fiche_effecteur_col2_label,
			codeExecLabel : ENGINE_LABELS.fiche_effecteur_code_execution_label,
			gardeLabel : ENGINE_LABELS.fiche_effecteur_men_garde_label,
			menDateLabel : ENGINE_LABELS.fiche_effecteur_debut_int_label,
			menDateFinLabel : ENGINE_LABELS.fiche_effecteur_fin_int_label,
			hasRemarque : function(){
				return this.menDTO.moyenSecoursDTO.remarque && this.menDTO.moyenSecoursDTO.remarque.trim()
			},
			isPhoneListDisplayed : function(){
				var test = (this.menDTO.moyenSecoursDTO.fax || this.menDTO.moyenSecoursDTO.moyenTelDTOs);
				return test != undefined && test !== null;
			},
			getClassCodeExecution : function(){
				return DecisionUtils.getInstance().getClassCodeExec(this.menDTO.menOk);
			},
			getCodeExec : function(){
				return this.menDTO.menCode ? this.menDTO.menCode : "?";
			},
			isMenGarde : function(){
				return this.menDTO.menGarde === "1";
			}
		};
	this.alive = true;
	this.title = null;
	this.infoGenerale = null;
	this.generalHtml = null;

	/**
	 * Initialise les données d'une fiche pour le moyen engagé courant
	 * @private
	 */
	var init = function(){
		setDTO(dto);
		generateTitle();
		generateInfoGenerale();
		generateGeneralHtml();
	};

	/**
	 * Set current dto
	 * @private
	 */
	var setDTO = function(dto){
		if (dto.moyenSecoursDTO){
			dto.moyenSecoursDTO.formatedFax = dto.moyenSecoursDTO.fax ? FieldsUtils.Tel.format(dto.moyenSecoursDTO.fax) : "";
			if (dto.moyenSecoursDTO.moyenTelDTOs){
				for (var i=0,total=dto.moyenSecoursDTO.moyenTelDTOs.length; i<total; i++){
					setTelAttributes(dto.moyenSecoursDTO.moyenTelDTOs[i]);
				}
			}
		}
		menDTO = dto;
		tplData.menDTO = dto;
	};

	/**
	 * Parse moyenTel data to define cssClass, text format, tooltip
	 * @private
	 */
	var setTelAttributes = function(moyenTel){
		var  values = {};
		// Fax
		if (faxRegExp.test(moyenTel.remarque)){
			values.cssClass = "fiche-icone-fax";
			values.formatedTel = FieldsUtils.Tel.format(moyenTel.tel);
		}
		// Valeur numérique = Téléphone
		else if (!isNaN(parseInt(moyenTel.tel))){
			values.cssClass = "fiche-icone-telephone";
			values.tooltip = ENGINE_LABELS.fiche_effecteur_telephoner_label;
			values.formatedTel = FieldsUtils.Tel.format(moyenTel.tel);
		}
		// Email
		else if (moyenTel.tel.indexOf("@") !== -1){
			values.cssClass = "fiche-icone-email";
			values.tooltip = ENGINE_LABELS.fiche_effecteur_ecrire_label;
		}
		// Sinon autre: rien de particulier

		// Set values to DTO
		moyenTel.cssClass = values.cssClass ? values.cssClass : "";
		moyenTel.tooltip = values.tooltip ? values.tooltip : "";
		moyenTel.formatedTel = values.formatedTel ? values.formatedTel : moyenTel.tel;
	};

	/**
	 * Get text displayed on button
	 * @private
	 */
	var generateTitle = function(){
		_this.title = menDTO.menNom; //(menDTO.menNom + " - " + menDTO.moyenSecoursDTO.commune);
	};

	/**
	 * Generate text displayed between button and tabs
	 * @private
	 */
	var generateInfoGenerale = function(){
		var mse = menDTO.moyenSecoursDTO,
			ligne1 = (mse.qualification ? mse.qualification + ", " : "") + (mse.type ? mse.type : "");
		_this.infoGenerale = {
			ligne1 : (ligne1 ? ligne1 + " - " : "") + menDTO.menNom + " - ",
			ligne2 :
				  (mse.numVoie    ? mse.numVoie + " "    : "")
				+ (mse.nomVoie    ? mse.nomVoie + ", "   : "")
				+ (mse.etare      ? mse.etare + ", "     : "")
				+ (mse.codePostal ? mse.codePostal + " " : "")
				+ (mse.commune    ? mse.commune          : "")
		};
		tplData.infoGenerale = _this.getInformationHtml();
	};

	/**
	 * Return info general label for title attribute
	 * @override
	 */
	this.getInformationTooltip = function(){
		return _this.infoGenerale.ligne1 + _this.infoGenerale.ligne2;
	};

	/**
	 * Return html content for info generale
	 * @override
	 */
	this.getInformationHtml = function(){
		return /*"<div>" + _this.infoGenerale.ligne1 + "</div>&nbsp;" +*/ "<div>Adresse : " + _this.infoGenerale.ligne2 + "</div>";
	};

	/**
	 * Generate HTML content for "General" tab
	 * @private
	 */
	var generateGeneralHtml = function(){
		_this.generalHtml = ich.tplFicheMenGeneral(tplData);
	};

	/**
	 * Get button label
	 */
	this.getTitle = function(){
		return _this.title;
	};

	/**
	 * Returns HTML content for "General" tab
	 */
	this.getGeneralHtml = function(){
		return $("<div />").html(_this.generalHtml).html();
	};

	init();
}
