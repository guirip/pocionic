/**
 * Fiche d'un moyen engagé
 * @author GRI
 * @param dto
 * @returns
 */
function FicheMoyenEngageHelper(dto, $scope) {
	"use strict";

	var _this = this,
		menDTO,
		faxRegExp = new RegExp("[Ff][Aa][Xx]");

	$scope.col1Label = ENGINE_LABELS.fiche_effecteur_col1_label;
	$scope.derniereDispoLabel = ENGINE_LABELS.fiche_effecteur_derniere_dispo_label;
	$scope.telephonesLabel = ENGINE_LABELS.fiche_effecteur_telephones_label;
	$scope.faxLabel = ENGINE_LABELS.fiche_effecteur_fax_label;
	$scope.col2Label = ENGINE_LABELS.fiche_effecteur_col2_label;
	$scope.codeExecLabel = ENGINE_LABELS.fiche_effecteur_code_execution_label;
	$scope.gardeLabel = ENGINE_LABELS.fiche_effecteur_men_garde_label;
	$scope.menDateLabel = ENGINE_LABELS.fiche_effecteur_debut_int_label;
	$scope.menDateFinLabel = ENGINE_LABELS.fiche_effecteur_fin_int_label;
	$scope.hasRemarque = function(){
		return this.menDTO.moyenSecoursDTO.remarque && this.menDTO.moyenSecoursDTO.remarque.trim()
	};
	$scope.isPhoneListDisplayed = function(){
		var test = (this.menDTO.moyenSecoursDTO.fax || this.menDTO.moyenSecoursDTO.moyenTelDTOs);
		return test != undefined && test !== null;
	};
	$scope.getClassCodeExecution = function(){
		return DecisionUtils.getInstance().getClassCodeExec(this.menDTO.menOk);
	};
	$scope.getCodeExec = function(){
		return this.menDTO.menCode ? this.menDTO.menCode : "?";
	};
	$scope.isMenGarde = function(){
		return this.menDTO.menGarde === "1";
	};

	this.alive = true;
	this.title = null;
	this.infoGenerale = null;
	this.generalHtml = null;

	/**
	 * Initialise les données d'une fiche pour le moyen engagé courant
	 * @private
	 */
	var init = function(){
		setDTO(dto);
		generateTitle();
		generateInfoGenerale();
		generateGeneralHtml();
	};

	/**
	 * Set current dto
	 * @private
	 */
	var setDTO = function(dto){
		if (dto.moyenSecoursDTO){
			dto.moyenSecoursDTO.formatedFax = dto.moyenSecoursDTO.fax ? FieldsUtils.Tel.format(dto.moyenSecoursDTO.fax) : "";
			if (dto.moyenSecoursDTO.moyenTelDTOs){
				for (var i=0,total=dto.moyenSecoursDTO.moyenTelDTOs.length; i<total; i++){
					setTelAttributes(dto.moyenSecoursDTO.moyenTelDTOs[i]);
				}
			}
		}
		menDTO = dto;
		$scope.menDTO = dto;
	};

	/**
	 * Parse moyenTel data to define cssClass, text format, tooltip
	 * @private
	 */
	var setTelAttributes = function(moyenTel){
		var  values = {};
		// Fax
		if (faxRegExp.test(moyenTel.remarque)){
			values.cssClass = "fiche-icone-fax";
			values.formatedTel = FieldsUtils.Tel.format(moyenTel.tel);
		}
		// Valeur numérique = Téléphone
		else if (!isNaN(parseInt(moyenTel.tel))){
			values.cssClass = "fiche-icone-telephone";
			values.tooltip = ENGINE_LABELS.fiche_effecteur_telephoner_label;
			values.formatedTel = FieldsUtils.Tel.format(moyenTel.tel);
		}
		// Email
		else if (moyenTel.tel.indexOf("@") !== -1){
			values.cssClass = "fiche-icone-email";
			values.tooltip = ENGINE_LABELS.fiche_effecteur_ecrire_label;
		}
		// Sinon autre: rien de particulier

		// Set values to DTO
		moyenTel.cssClass = values.cssClass ? values.cssClass : "";
		moyenTel.tooltip = values.tooltip ? values.tooltip : "";
		moyenTel.formatedTel = values.formatedTel ? values.formatedTel : moyenTel.tel;
	};

	/**
	 * Get text displayed on button
	 * @private
	 */
	var generateTitle = function(){
		_this.title = (menDTO.menNom + " - " + menDTO.moyenSecoursDTO.commune);
	};

	/**
	 * Return info general label for title attribute
	 * @override
	 */
	this.getInformationTooltip = function(){
		return _this.infoGenerale.ligne1 + _this.infoGenerale.ligne2;
	};

	/**
	 * Return html content for info generale
	 * @override
	 */
	this.getInformationHtml = function(){
		return "<div>" + _this.infoGenerale.ligne1 + "</div>&nbsp;<div>" + _this.infoGenerale.ligne2 + "</div>";
	};

	/**
	 * Generate HTML content for "General" tab
	 * @private
	 */
	var generateGeneralHtml = function(){
		_this.generalHtml = ich.tplFicheMenGeneral(tplData);
	};

	/**
	 * Get button label
	 */
	this.getTitle = function(){
		return _this.title;
	};

	/**
	 * Returns HTML content for "General" tab
	 */
	this.getScope = function(){
		return $scope;
	};

	init();
}
