
- Installer NodeJS
- Installer Ionic et Cordova : npm install -g cordova ionic

- Installer Apache Ant

- Installer le SDK android
- Copier/coller le programme sdk\build-tools\android-4.4W\zipalign.exe dans sdk\tools

- Créer les variables d'envrionnement :
	ANDROID_HOME = C:\Program Files\adt-bundle-windows-x86_64-20140702\sdk
	ANT_HOME = C:\Program Files (x86)\apache-ant-1.9.4
	ajouter à la variable Path : %ANT_HOME%\bin;%ANDROID_HOME%\tools;%ANDROID_HOME%\platform-tools;%ANDROID_HOME%\build-tools\android-4.4W

- La commande "android" (sans argument) lance l'interface de gestion des packages (voir android-packages.png)
  et permet de définir un émulateur, ce qui ne parait pas indispensable vu sa très forte lenteur (voir android-emulateur.png)



- Créer un nouveau projet IONIC (se faire un workspace à l'extérieur du répertoire d'install de node)
  	ionic start monProjet blank
	cd monProjet
	ionic platform add android

  blank signifie projet vierge, sinon des templates sont utilisables (tabs, sidemenu, etc)
  la commande "platform add" ne sert pas dans l'immédiat mais permettra plus tard de builder le projet pour une plate-forme



- Tester un projet  
    ionic serve
    
  cette dernière commande permet également de développer avec un rechargement automatique à chaque sauvegarde de fichier.



- Lancer l'émulateur android (si la lenteur ne vous effraie pas) : 
	ionic build android
	ionic emulate android



- Faire un build pour android (source : http://ionicframework.com/docs/guide/publishing.html) :

    - Attention à l'url configurée si accès à un middleware !

    - Pour alléger le livrable, on peut supprimer le plugin de sortie console de cordova :
      cordova plugin rm org.apache.cordova.console

    - Pour un build de type "release", modifier le fichier platforms/android/AndroidManifest.xml
      modifier ou ajouter au noeud "application" l'attribut:  android:debuggable="false" 

	- Générer le livrable :
	  cordova build --release android
	  cd platforms/android/ant-build

	- voir le lien ci-dessus pour générer une clé privée

	- Signer le livrable (-keystore reçoit le chemin vers le fichier keystore. Le dernier argument est l'alias du keystore) :
	  jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore G:\gri-release-key.keystore pocIonic-release-unsigned.apk gri

	- Optimize the apk :
	  Supprimer l'ancien apk généré s'il exsite, snino zipalign va échouer : del pocIonic.apk
	  zipalign -v 4 pocIonic-release-unsigned.apk pocIonic.apk

    - Copier le fichire apk sur le téléphone et l'exécuter pour installer l'application


